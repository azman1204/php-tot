<?php
// sample loop
for($i=0; $i<10; $i++) {
    echo "No $i <br>";
}

// foreach loop, khas utk array
$buah = ['rambutan', 'durian', 'langsat'];
// tiap2 kali loop, ambil satu val dari array dan assign ke $b 
foreach($buah as $b) {
    echo "$b <br>";
}