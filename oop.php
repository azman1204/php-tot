<?php
// class, object
// class ada 2 perkra: 1. property, 2. methods

class Person {
    // properties (public, private, protected)
    public $name;
    public $address;

    // methods
    function printInfo() {
        return "my name is $this->name and my address is $this->address";
    }
}

// inheritance
class Student extends Person {
    public $matrixno;
    public $cgpa;

    function printFinalResult() {
        return "Name = $this->name, CGPA = $this->cgpa";
    }
}

$stu1 = new Student();
$stu1->name = 'John Doe';
$stu1->cgpa = 3.5;
echo $stu1->printFinalResult() . '<br>';

// create object
$p1 = new Person();
$p1->name = "azman";
$p1->address = "bangi";
echo $p1->printInfo() . '<br>';

$p2 = new Person();
$p2->name = 'Abu';
$p2->address = 'KL';
echo $p2->printInfo() . '<br>';