<?php
// $no1, $no2 - parameter
// $no3 - optional parameter, ada default value
function tambah($no1, $no2, $no3 = 0) {
    $jum = $no1 + $no2 + $no3;
    return $jum;
}

// 1, 3 - arguments
echo "1 + 3 = " . tambah(1,3);
echo '<br>';
echo "1 + 2 + 3 = " . tambah(1,2,3);